package org.coffin.spatialindex.lib;

import org.slf4j.Logger;

/**
 * @author eric.coffin@unb.ca
 * Some basic shared functions.
 */
public class ContainerHelper {

    /**
     * Log number of processors and memory in bytes.
     * Uses info level.
     * @param logger logger to write to.
     */
    public void logRuntimeSpecs(Logger logger){
        Runtime runtime = Runtime.getRuntime();
        int processors = runtime.availableProcessors();
        long maxMemory = runtime.maxMemory();
        logger.info(String.format("Number of processors: %d\n", processors));
        logger.info(String.format("Max memory: %d bytes\n", maxMemory));
    }
}
