package org.coffin.spatialindex.rindex;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import io.grpc.stub.StreamObserver;
import org.coffin.spatialindex.Envelope;
import org.coffin.spatialindex.GetEnvelopeParams;
import org.coffin.spatialindex.IndexResponse;
import org.coffin.spatialindex.RIndexGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * @author eric.coffin@unb.ca
 *
 * RIndexServer accepts requests for getEnvelope and performLookup.
 *
 * Maintains an in-memory RTree of a set of features.
 * 
 * Implemented on gRPC (for the proto file see src/proto/spatial_index.proto).
 *
 * main() method requires a single parameter - the path to a shape file (this same folder must contain
 * the associated shx file).
 */
public class RIndexServer {

    private static final Logger logger = LoggerFactory.getLogger(RIndexServer.class);

    private Server server;
    private static RIndex index;


    private void start() throws IOException {
        // The port on which the server should run
        int port = 50052;
        server = ServerBuilder.forPort(port)
                .addService(new RIndexImpl())
                .build()
                .start();
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                RIndexServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }


    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Task runner.
     * First argument must be path to shape file.
     */
    public static void main(String[] args) throws Exception {


        if (args.length != 1) {
            System.err.println("Shape file argument is missing. Exiting...");
            System.exit(1);
        }

        Runtime runtime = Runtime.getRuntime();

        int processors = runtime.availableProcessors();
        long maxMemory = runtime.maxMemory();

        logger.info(String.format("Number of processors: %d\n", processors));
        logger.info(String.format("Max memory: %d bytes\n", maxMemory));

        String shapePath = args[0];

        File file = new File(shapePath);

        index = new RIndex(file);
        index.load();

        final RIndexServer server = new RIndexServer();
        server.start();
        server.blockUntilShutdown();

    }

    static class RIndexImpl extends RIndexGrpc.RIndexImplBase {

        @Override
        public void performLookup(Envelope request, StreamObserver<IndexResponse> responseObserver) {
            List result = index.getRtree().query(new org.locationtech.jts.geom.Envelope(
                    request.getMinX(), request.getMaxX(), request.getMinY(), request.getMaxY()
            ));
            IndexResponse.Builder responseBuilder = IndexResponse.newBuilder();
            for (int i = 0; i < result.size(); i++) {
                responseBuilder.addId( (String) ((org.locationtech.jts.geom.MultiPolygon) result.get(i)).getUserData());
            }
            responseObserver.onNext(responseBuilder.build());
            responseObserver.onCompleted();
        }

        @Override
        public void getEnvelope(GetEnvelopeParams request, StreamObserver<Envelope> responseObserver) {
            responseObserver.onNext(index.getEnvelope());
            responseObserver.onCompleted();
        }
    }


}
