
package org.coffin.spatialindex.gindex;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.coffin.spatialindex.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


/**
 * @author eric.coffin@unb.ca
 * Performs forwarding / Proxy operation to RIndex for GIndex.
 */
public class RIndexClient {
    private static final Logger logger = LoggerFactory.getLogger(RIndexClient.class);

    private final ManagedChannel channel;
    private final RIndexGrpc.RIndexBlockingStub blockingStub;

    /**
     * Construct RIndexClient using host and port
     */
    public RIndexClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build());
    }

    /**
     * Construct RIndexClient using channel.
     */
    RIndexClient(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = RIndexGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public IndexResponse performLookup(Envelope envelope) {
        return blockingStub.performLookup(envelope);

    }

    public Envelope getEnvelope() {
        return blockingStub.getEnvelope(GetEnvelopeParams.getDefaultInstance());
    }

}
